package com.keagan.prac.tables;

import com.keagan.prac.model.Song;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SongsTable extends JpaRepository<Song, Long> {
    @Query(nativeQuery = true, value = "select * from songs where title like ? and artist like ?")
    List<Song> findByQuery(String title, String artist);

    @Query(nativeQuery = true, value = "select id from songs limit 1;")
    Long getFirstColumn();
}
