package com.keagan.prac.model;

import java.util.ArrayList;
import java.util.List;

public class TrackList {

    private List<Song> tracks;

    public TrackList() {
        this.tracks = new ArrayList<>();
    }

    public TrackList(List<Song> tracks) {
        this.tracks = tracks;
    }

    public List<Song> getTracks() {
        return tracks;
    }

    public void setTracks(List<Song> tracks) {
        this.tracks = tracks;
    }

    public boolean isEmpty() {
        return tracks.isEmpty();
    }
}
