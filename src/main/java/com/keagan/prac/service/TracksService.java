package com.keagan.prac.service;

import com.keagan.prac.model.Song;
import com.keagan.prac.model.SongNotFoundException;
import com.keagan.prac.model.TrackList;
import com.keagan.prac.tables.SongsTable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TracksService {

    SongsTable songsTable;

    public TracksService(SongsTable songsTable) {
        this.songsTable = songsTable;
    }

    public TrackList getTracks() {
        List<Song> foundSongs = songsTable.findAll();
        if (foundSongs.isEmpty()) return null;
        return new TrackList(foundSongs);
    }

    public TrackList getTracks(String title, String artist) {
        List<Song> foundSongs = songsTable.findByQuery(title + '%', artist + '%');
        if (foundSongs.isEmpty()) return null;
        return new TrackList(foundSongs);
    }

    public Song addTrack(Song s) {
        return songsTable.save(s);
    }

    public Song getTrackById(Long id) {
        Optional<Song> foundSong = songsTable.findById(id);
        return foundSong.orElse(null);
    }

    public Song updateTrackById(Long id, Song song) {
        Optional<Song> foundSong = songsTable.findById(id);
        if (foundSong.isPresent()) {
            song.setId(id);
            return songsTable.save(song);
        }
        return null;
    }

    public void deleteTrack(long id) {
        Optional<Song> s = songsTable.findById(id);
        if (s.isEmpty()) throw new SongNotFoundException();
        songsTable.delete(s.get());

    }
}
