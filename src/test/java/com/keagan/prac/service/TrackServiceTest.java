package com.keagan.prac.service;

import com.keagan.prac.model.Song;
import com.keagan.prac.model.SongNotFoundException;
import com.keagan.prac.model.TrackList;
import com.keagan.prac.tables.SongsTable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TrackServiceTest {

    List<Song> testTracks;
    private TracksService tracksService;

    @Mock
    private SongsTable songsTable;

    @BeforeEach
    void setup() {
        tracksService = new TracksService(songsTable);
        testTracks = new ArrayList<>();
        Song s = new Song("title", "artist", "album");
        testTracks.add(s);
    }

    @Test
    void getTracks_NoParams_ReturnTrackList() {
        when(songsTable.findAll()).thenReturn(testTracks);

        TrackList trackList = tracksService.getTracks();
        assertNotNull(trackList);
        assertEquals(testTracks.size(), trackList.getTracks().size());
    }

    @Test
    void getTracks_NoParams_ReturnNull() {
        when(songsTable.findAll()).thenReturn(new ArrayList<>());

        TrackList trackList = tracksService.getTracks();
        assertNull(trackList);
    }

    @Test
    void getTracks_Title_ReturnTrackListWithTitles() {
        when(songsTable.findByQuery(anyString(), anyString())).thenReturn(testTracks);

        TrackList trackList = tracksService.getTracks("title", "");
        assertNotNull(trackList);
        assertEquals(testTracks.size(), trackList.getTracks().size());
    }

    @Test
    void getTracks_Artist_ReturnTrackListWithTitles() {
        when(songsTable.findByQuery(anyString(), anyString())).thenReturn(testTracks);

        TrackList trackList = tracksService.getTracks("", "artist");
        assertNotNull(trackList);
        assertEquals(testTracks.size(), trackList.getTracks().size());
    }

    @Test
    void getTracks_TitleAndArtist_ReturnTrackListWithTitles() {
        when(songsTable.findByQuery(anyString(), anyString())).thenReturn(testTracks);

        TrackList trackList = tracksService.getTracks("title", "artist");
        assertNotNull(trackList);
        assertEquals(testTracks.size(), trackList.getTracks().size());
    }

    @Test
    void getTracks_TitleAndArtist_ReturnNull() {
        when(songsTable.findByQuery(anyString(), anyString())).thenReturn(new ArrayList<>());

        TrackList trackList = tracksService.getTracks("title", "artist");
        assertNull(trackList);
    }

    @Test
    void addTrack_Track_ReturnTrack() {
        Song s = new Song("title", "artist", "album");
        when(songsTable.save(any(Song.class))).thenReturn(s);

        Song returnedSong = tracksService.addTrack(s);
        assertNotNull(returnedSong);
        assertEquals(s.getTitle(), returnedSong.getTitle());
    }

    @Test
    void getTrackById_Id_ReturnsTrack() {
        Song s = new Song("title", "artist", "album");
        when(songsTable.findById(anyLong())).thenReturn(Optional.of(s));
        Song foundSong = tracksService.getTrackById(12L);

        assertNotNull(foundSong);
        assertEquals(s.getTitle(), foundSong.getTitle());
    }

    @Test
    void getTrackById_Id_ReturnsNull() {
        when(songsTable.findById(anyLong())).thenReturn(Optional.empty());

        Song foundSong = tracksService.getTrackById(12L);
        assertNull(foundSong);
    }

    @Test
    void updateTrackById_IdAndSong_ReturnsUpdatedTrack() {
        Song s = testTracks.get(0);
        when(songsTable.findById(anyLong())).thenReturn(Optional.of(s));
        when(songsTable.save(any(Song.class))).thenReturn(s);

        Song updated = tracksService.updateTrackById(12L, s);
        assertNotNull(updated);
        assertEquals(12L, s.getId());
    }

    @Test
    void updateTrackById_IdAndSong_ReturnsNull() {
        when(songsTable.findById(anyLong())).thenReturn(Optional.empty());

        Song updated = tracksService.updateTrackById(12L, testTracks.get(0));
        assertNull(updated);
    }

    @Test
    void deleteTrack_Id_Success() {
        long id = 12;
        when(songsTable.findById(anyLong())).thenReturn(Optional.of(testTracks.get(0)));
        tracksService.deleteTrack(id);
        verify(songsTable).delete(any(Song.class));
    }

    @Test
    void deleteTrack_Id_Exception() {
        long id = 12;
        when(songsTable.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(SongNotFoundException.class,
                () -> tracksService.deleteTrack(id));
    }
}
