package com.keagan.prac;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.keagan.prac.model.Song;
import com.keagan.prac.model.TrackList;
import com.keagan.prac.tables.SongsTable;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PracApplicationTests {

    private final String URL = "/api/music";
    @Autowired
    TestRestTemplate restTemplate;
    @Autowired
    SongsTable songsTable;

    private List<Song> testTracks;

    @BeforeEach
    void setup() {
        this.testTracks = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            int j = i % 10;
            Song s = new Song("title" + j, "artist" + j, "album" + j);
            testTracks.add(s);
        }
        songsTable.saveAll(this.testTracks);
    }

    @AfterEach
    void teardown() {
        songsTable.deleteAll();
    }

    @Test
    void contextLoads() {
    }

    @Test
    void getTracks_None_ReturnsTrackList() {
        ResponseEntity<TrackList> response = restTemplate.getForEntity(URL, TrackList.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(50, response.getBody().getTracks().size());
    }

    @Test
    void getTracks_None_NoContent() {
        songsTable.deleteAll();
        ResponseEntity<TrackList> response = restTemplate.getForEntity(URL, TrackList.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void getTracks_Title_ReturnsTrackListWithTitles() {
        String query = "?title=title1";
        ResponseEntity<TrackList> response = restTemplate.getForEntity(URL + query, TrackList.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(5, response.getBody().getTracks().size());
    }

    @Test
    void getTracks_Title_ReturnsNoContent() {
        String query = "?title=notFound1";
        ResponseEntity<TrackList> response = restTemplate.getForEntity(URL + query, TrackList.class);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void getTracks_Artist_ReturnsTrackListWithArtists() {
        String query = "?artist=artist1";
        ResponseEntity<TrackList> response = restTemplate.getForEntity(URL + query, TrackList.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(5, response.getBody().getTracks().size());
    }

    @Test
    void getTracks_Artist_ReturnsNoContent() {
        String query = "?artist=notFound1";
        ResponseEntity<TrackList> response = restTemplate.getForEntity(URL + query, TrackList.class);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void getTracks_TitleAndArtist_ReturnsTrackListWithTitlesAndArtist() {
        String query = "?title=title1&artist=artist1";
        ResponseEntity<TrackList> response = restTemplate.getForEntity(URL + query, TrackList.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(5, response.getBody().getTracks().size());
    }

    @Test
    void getTracks_TitleAndArtist_ReturnsNoContent() {
        String query = "?title=title1&artist=artist2000";
        ResponseEntity<TrackList> response = restTemplate.getForEntity(URL + query, TrackList.class);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void addTrack_Song_ReturnsCreatedSong() throws JsonProcessingException {
        String body = toJSON(new Song("title100", "artist100", "album100"));

        ResponseEntity<Song> response = restTemplate.postForEntity(URL, getHeaders(body), Song.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("artist100", response.getBody().getArtist());
    }

    @Test
    void addTrack_Song_ReturnsBadRequest() throws JsonProcessingException {
        String body = toJSON(new Song("", "artist100", "album100"));

        ResponseEntity<Song> response = restTemplate.postForEntity(URL, getHeaders(body), Song.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void addTrack_Song_ReturnsBadRequest2() throws JsonProcessingException {
        String body = toJSON(new Song("rush", null, "album100"));

        ResponseEntity<Song> response = restTemplate.postForEntity(URL, getHeaders(body), Song.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void getTrack_Id_returnsSong() {
        String path = "/1";

        ResponseEntity<Song> response = restTemplate.getForEntity(URL + path, Song.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().getId());
    }

    @Test
    void getTrack_Id_NoContent() {
        String path = "/10001";

        ResponseEntity<Song> response = restTemplate.getForEntity(URL + path, Song.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void updateTrack_IdAndSong_ReturnsUpdatedSong() throws JsonProcessingException {
        Long id = songsTable.getFirstColumn();
        String newSong = toJSON(new Song("black sabbath", "black sabbath", "black sabbath"));
        String path = "/" + id;

        ResponseEntity<Song> oldSong = restTemplate.getForEntity(URL + path, Song.class);
        restTemplate.put(URL + path, getHeaders(newSong), Song.class);
        ResponseEntity<Song> updatedSong = restTemplate.getForEntity(URL + path, Song.class);

        assertEquals(HttpStatus.OK, oldSong.getStatusCode());
        assertEquals(HttpStatus.OK, updatedSong.getStatusCode());

        assertNotNull(oldSong.getBody());
        assertNotNull(updatedSong.getBody());

        assertEquals(oldSong.getBody().getId(), updatedSong.getBody().getId());
        assertEquals("black sabbath", updatedSong.getBody().getTitle());
        assertEquals("black sabbath", updatedSong.getBody().getArtist());
        assertEquals("black sabbath", updatedSong.getBody().getAlbum());
    }

    @Test
    void updateTrack_IdAndSong_NoContent() throws JsonProcessingException {
        String newSong = toJSON(new Song("black sabbath", "black sabbath", "black sabbath"));
        String path = "/1000900090";

        ResponseEntity<Song> oldSong = restTemplate.getForEntity(URL + path, Song.class);
        restTemplate.put(URL + path, getHeaders(newSong), Song.class);

        assertEquals(HttpStatus.NO_CONTENT, oldSong.getStatusCode());
    }

    @Test
    void updateTrack_IdAndSong_BadRequest() throws JsonProcessingException {
        Long id = songsTable.getFirstColumn();
        String newSong = toJSON(new Song("", null, "black sabbath"));
        String path = "/" + id;

        ResponseEntity<Song> oldSong = restTemplate.getForEntity(URL + path, Song.class);
        restTemplate.put(URL + path, getHeaders(newSong), Song.class);
        ResponseEntity<Song> sameSong = restTemplate.getForEntity(URL + path, Song.class);

        assertEquals(HttpStatus.OK, oldSong.getStatusCode());
        assertEquals(HttpStatus.OK, sameSong.getStatusCode());

        assertNotNull(oldSong.getBody());
        assertNotNull(sameSong.getBody());

        assertEquals(oldSong.getBody().getId(), sameSong.getBody().getId());
        assertEquals(oldSong.getBody().getTitle(), sameSong.getBody().getTitle());
        assertEquals(oldSong.getBody().getArtist(), sameSong.getBody().getArtist());
        assertEquals(oldSong.getBody().getAlbum(), sameSong.getBody().getAlbum());
    }

    @Test
    void deleteTrack_Id_Accepted() {
        Long id = songsTable.getFirstColumn();
        String path = "/" + id;

        ResponseEntity<Song> oldSong = restTemplate.getForEntity(URL + path, Song.class);
        restTemplate.delete(URL + path);
        ResponseEntity<Song> deletedSong = restTemplate.getForEntity(URL + path, Song.class);

        assertEquals(HttpStatus.OK, oldSong.getStatusCode());
        assertEquals(HttpStatus.NO_CONTENT, deletedSong.getStatusCode());

        assertNotNull(oldSong.getBody());
        assertEquals(id, oldSong.getBody().getId());
    }

    @Test
    void deleteTrack_Id_NoContent() {
        String path = "/1999999991";
        ResponseEntity<Song> response = restTemplate.getForEntity(URL + path, Song.class);
        restTemplate.delete(URL + path);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }


    private String toJSON(Object auto) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(auto);
    }

    private HttpEntity<?> getHeaders(String body) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        return new HttpEntity<>(body, headers);
    }
}
